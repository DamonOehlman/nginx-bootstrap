# initialise defaults
NODEPATH=

bootstrap: check node_modules
	@echo "Preparing site: ${SITE}"
	@mkdir -p public/${SITE}
	@`npm bin`/formatter \
		--SITE=${SITE} \
		< conf/templates/${TEMPLATE} \
		> conf/sites/${SITE}

clean: check
	@echo "Removing ${SITE} configuration"
	@rm -f conf/sites/${SITE}

check:
	@${NODEPATH}node --version > /dev/null # check that we have node

	@if [ "${SITE}" = "" ] ; then \
		echo "Need to specify SITE"; exit 1 ; \
	fi

	@if [ "${TEMPLATE}" = "" ] ; then \
		echo "Need to specify TEMPLATE"; exit 1 ; \
	fi

node_modules:
	@${NODEPATH}npm install