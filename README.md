# nginx-bootstrap

This is a simple nginx configuration bootstrap that is designed for use with [bashinate](https://github.com/DamonOehlman/bashinate).

## Scaffolding a new site

This repository includes a `Makefile` that can be used to scaffold configurations for new sites. A number of templates have been gathered from around the web, and scaffolding a site is as simple as the following:

```
SITE=yourdomain.com TEMPLATE=nodeapp make
```

__NOTE__:  This process uses a tool called [formatter](https://github.com/DamonOehlman/formatter) which is built using node.  As such scaffolding will not work without [node](http://nodejs.org) and `npm` installed and available on the machine.

If `node` is installed on a path other than the default, specify the `NODEPATH` as an additional environment variable.  For example:

```
NODEPATH=/opt/local/node/v0.10.15/ SITE=yourdomain.com TEMPLATE=php make
```